# Lancer l'app

## Basculer sur la branche vuln
- git switch vuln
- cd symfony-vuln/  

## Installer l'app
- composer update  

## Installer la DB
- créer un fichier .env.local avec les informations de votre DB en local  
exemple :  
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"  
(voir le fichier .env pour voir les exemples)  

- rentrer les commandes suivantes :  
php bin/console do:da:cr  
php bin/console do:sc:up --force  

## Lancer un serveur
- rentrer la commande :  
php -S localhost:8000 -t public


# Corriger les erreurs

## XSS

Pour ne plus avoir la faille xss, il faut supprimer le **|raw** après la variable dans le twig, symfony escape automatiquement les caractères spéciaux.  

## SQLI
Plusieurs solutions avec symfony:
- Faire une requête **DQL** plutôt que SQL
- Si on laisse la requête SQL, il faut mettre les variables présentes dans la reqûete en **paramètre** de la méthode executeQuery (exemple : executeQuery($sql, array("id" => id, "name => $name)))
